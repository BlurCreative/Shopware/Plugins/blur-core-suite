<?php declare(strict_types=1);

namespace Blur\CoreSuite\Twig\Extensions;

use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use Blur\CoreSuite\Services\Category as CategoryServices;
use Shopware\Core\Content\Category\CategoryEntity;

class GetCategoryById extends AbstractExtension
{
    /**
     * @var CategoryServices
     */
    private $categoryServices;

    public function __construct( 
        CategoryServices $categoryServices
    )
    {
        $this->categoryServices = $categoryServices;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction( 
                'get_category_by_id', 
                [$this, 'fetchCategory'], 
                ['needs_context' => true]
            ),
        ];
    }

    public function fetchCategory(
        array $twigContext, 
        string $categoryID, 
        ?SalesChannelContext $salesChannelContext = null
    ): CategoryEntity
    {
        if ( $salesChannelContext === null && array_key_exists('context', $twigContext) && $twigContext['context'] instanceof SalesChannelContext) {
            $salesChannelContext = $twigContext['context']->getContext();

            /**
             * @TODO
             * context bug.
             * the response here is @var Content but should be @var SalesChannelContext
            **/
        }

        return $this->categoryServices->getCategory( $categoryID, $salesChannelContext );
    }
}