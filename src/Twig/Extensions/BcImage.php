<?php declare(strict_types=1);

namespace Blur\CoreSuite\Twig\Extensions;

use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use Shopware\Core\Content\Media\Aggregate\MediaThumbnail\MediaThumbnailCollection;

class BcImage extends AbstractExtension
{

    public $thumbnailSizes;

    public $sizes;

    public function __construct( 
    )
    {
        $this->sizes = [];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction( 
                'bc_image', 
                [$this, 'buildImageTag'], 
                ['needs_context' => true]
            ),
        ];
    }

    public function buildImageTag(
        array $twigContext, 
        MediaThumbnailCollection $thumbnails,
        array $thumbnailSizes = null,
        array $options = null
    ): ?string
    {
        $this->thumbnailSizes = $thumbnailSizes;

        $markup = '<picture >';
        $imageSets = $this->getSizes( $thumbnails, $thumbnailSizes );
        $imageSrcsets = $this->buildSrcsets( $imageSets );

        if ( $imageSets["default"] ) {
            $markup .= '<img';
            $markup .= ' srcset="' . $imageSets["default"]->getUrl() . '"';
            if ( $options["imgClass"] ) {
                $markup .= ' class="'. trim( $options["imgClass"] ) . '"';
            }
            if ( $options["dimensionMarkup"] === true ) {
                $markup .= ' width="' . $imageSets["default"]->getWidth() . '"';
                $markup .= ' height="' . $imageSets["default"]->getHeight() . '"';
            }
            $markup .= '/>';
        }

        $markup .= '</picture>';
        
        return $markup;
    }

    private function getSizes(
        MediaThumbnailCollection $thumbnails = null
    ): ?array
    {
        array_walk_recursive( $thumbnails, function( $item, $key ) {
            $size = array_search( $item->getWidth(), $this->thumbnailSizes );

            if ( in_array( $item->getWidth(), $this->thumbnailSizes ) === true ) {
                $this->sizes[ $size ] = $item;
            }
        } );

        return $this->sizes;
    }

    private function buildSrcsets(
        array $imageSets
    ): ?string
    {
        $markup = '';

        foreach ( $imageSets as $key => $value ) {
            if ( $key !== 'default' ) {
                # @TODO build srcset markup
            }
        }

        return $markup;
    }
}