import template from "./sw-order-line-items-grid.html.twig";

Shopware.Component.override( 'sw-order-line-items-grid', {
    template,

    created: function () {
        this.getLineItemColumns.splice( 1, 0, {
            property: 'productNumber',
            label: 'sw-order.detailBase.columnProductNumber',
            allowResize: false,
            align: 'left',
            inlineEdit: true,
            width: '90px',
        });
    }
});