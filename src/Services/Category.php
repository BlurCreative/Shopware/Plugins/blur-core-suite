<?php declare(strict_types=1);

namespace Blur\CoreSuite\Services;

use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\System\SalesChannel\Entity\SalesChannelRepositoryInterface;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Content\Category\CategoryEntity;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\System\CustomField\CustomFieldTypes;
use Shopware\Core\Content\Category\Exception\CategoryNotFoundException;
use Shopware\Core\System\SalesChannel\SalesChannelContext;

class Category
{
    /**
     * @var SalesChannelRepositoryInterface
     */
    private $categoryRepository;

    public function __construct( 
        SalesChannelRepositoryInterface $categoryRepository
    )
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function getCategory( 
        string $categoryId, 
        SalesChannelContext $context,
        ?bool $fetchMedia = null
    ): CategoryEntity
    {
        $criteria = new Criteria(
            [ $categoryId ]
        );
        $criteria->setTitle( 'category::data' );

        if ( $fetchMedia === true ) {
            $criteria->addAssociation( 'media' );
        }
        
        $category = $this->categoryRepository
            ->search($criteria, $context)
            ->get($categoryId);

        if (!$category) {
            throw new CategoryNotFoundException($categoryId);
        }

        return $category;
    }
}